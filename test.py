#!/usr/bin/env python

import numpy as np


SEQ = 0
INIT = np.array([[3, 3], [0, 0], [0, 0]])
GOAL = np.array([[0, 0], [0, 0], [3, 3]])
BASE_MOVES = {'ltr': {'embark': [ np.zeros((3, 2), dtype=int),
                                  np.array([[-1, 0], [1, 0], [0, 0]]),
                                  np.array([[-2, 0], [2, 0], [0, 0]]),
                                  np.array([[0, -1], [0, 1], [0, 0]]),
                                  np.array([[0, -2], [0, 2], [0, 0]]),
                                  np.array([[-1, -1], [1, 1], [0, 0]])],
                      'disembark': [ np.zeros((3, 2), dtype=int),
                                     np.array([[1, 0], [-1, 0], [0, 0]]),
                                     np.array([[2, 0], [-2, 0], [0, 0]]),
                                     np.array([[0, 1], [0, -1], [0, 0]]),
                                     np.array([[0, 2], [0, -2], [0, 0]]),
                                     np.array([[1, 1], [-1, -1], [0, 0]])]},
              'rtl': {'embark': [ np.zeros((3, 2), dtype=int),
                                  np.array([[0, 0], [1, 0], [-1, 0]]),
                                  np.array([[0, 0], [2, 0], [-2, 0]]),
                                  np.array([[0, 0], [0, 1], [0, -1]]),
                                  np.array([[0, 0], [0, 2], [0, -2]]),
                                  np.array([[0, 0], [1, 1], [-1, -1]])],
                      'disembark': [ np.zeros((3, 2), dtype=int),
                                     np.array([[0, 0], [-1, 0], [1, 0]]),
                                     np.array([[0, 0], [-2, 0], [2, 0]]),
                                     np.array([[0, 0], [0, -1], [0, 1]]),
                                     np.array([[0, 0], [0, -2], [0, 2]]),
                                     np.array([[0, 0], [-1, -1], [1, 1]])]}}


def show_move(move):
    return ("{:-4d}{:-4d}| {:-4d}{:-4d}| {:-4d}{:-4d}".format(move[0][0],
        move[0][1],
        move[1][0],
        move[1][1],
        move[2][0],
        move[2][1]))


def show_situation(sit, boat_dir):
    return ("{:4s}{:4s}| {:4s}{:4s}| {:4s}{:4s}   ({})".format(sit[0, 0]*'H',
        sit[0, 1]*'T',
        sit[1, 0]*'H',
        sit[1, 1]*'T',
        sit[2, 0]*'H',
        sit[2, 1]*'T',
        boat_dir))


def check_visited(visited, sit, sit_from, move, boat_dir):
    for v in visited:
        if np.array_equal(v['sit'], sit):
            return True
    return False


def acceptable(sit, boat_dir):
    for l in sit:
        if l[0] < 0 or l[1] < 0:
            return False
        if l[0] > 0 and l[0] < l[1]:
            return False
    if np.sum(sit[1]) > 2:
        return False
    if (sit[1, 0] + sit[2, 0] < sit[1, 1] + sit[2, 1]) and (sit[1, 0] + sit[2, 0] > 0):
        return False
    if (sit[0, 0] + sit[1, 0] < sit[0, 1] + sit[1, 1]) and (sit[0, 0] + sit[1, 0] > 0):
        return False
    return True


def reachable(cur_sit, cur_sit_id, boat_dir, visited):
    global SEQ
    result = []
    for move_d in BASE_MOVES[boat_dir]['disembark']:
        if acceptable(cur_sit + move_d, boat_dir):
            if not check_visited(visited, cur_sit + move_d, cur_sit, move_d, boat_dir):
                SEQ += 1
                result.append({'from': cur_sit_id, 'id': SEQ, 'move': move_d, 'sit': cur_sit + move_d, 'boat': boat_dir})
            int_sit = cur_sit + move_d
            for move_e in BASE_MOVES[boat_dir]['embark']:
                if acceptable(int_sit + move_e, boat_dir) and not check_visited(visited, int_sit + move_e, int_sit, move_e, boat_dir):
                    SEQ += 1
                    result.append({'from': cur_sit_id, 'id': SEQ, 'move': move_d + move_e, 'sit': int_sit + move_e, 'boat': boat_dir})
    return result


def visit(cur_sit, this_id, previous, cur_move, boat_dir, visited, to_visit):
    visited.append({'from': previous, 'id': this_id, 'move': cur_move, 'sit': cur_sit, 'boat': boat_dir})
    to_visit.extend(reachable(cur_sit, this_id, boat_dir, visited))
    if np.array_equal(GOAL, cur_sit):
        return visited
    else:
        next_iter = to_visit.pop()
        next_sit = next_iter['sit']
        next_move = next_iter['move']
        next_id = next_iter['id']
        next_prev_id = next_iter['from']
        boat_dir = 'rtl' if boat_dir == 'ltr' else 'ltr'
        return visit(next_sit, next_id, next_prev_id, next_move, boat_dir, visited, to_visit)


if __name__ == '__main__':
    memory = visit(INIT, 0, None, None, 'ltr', [], [])
    cur_item = [m for m in memory if np.array_equal(m['sit'], GOAL)][0]
    result = []
    while cur_item['move'] is not None:
        result.insert(0, cur_item['move'])
        cur_item = [m for m in memory if m['id'] == cur_item['from']][0]
    cur_sit = INIT
    boat_dir = 0
    for m in result:
        print(show_situation(cur_sit, ['>', '<'][boat_dir]))
        boat_dir = 1 - boat_dir
        cur_sit += m
    print(show_situation(cur_sit, ['>', '<'][boat_dir]))
